-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-02-2021 a las 21:18:25
-- Versión del servidor: 10.4.16-MariaDB
-- Versión de PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `acuaterrario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `tipo` varchar(30) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `marca` varchar(30) NOT NULL,
  `clave` varchar(15) NOT NULL,
  `contenido` varchar(15) NOT NULL,
  `categoria` varchar(20) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `precio` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `tipo`, `nombre`, `marca`, `clave`, `contenido`, `categoria`, `descripcion`, `precio`) VALUES
(1, 'Peces', 'gfdsa', 'GFDSX', '', '', '', '', 0),
(2, 'Gatos', 'lkjmnhgbfd', 'jngbfvdcsx', '', '', '', '', 0),
(4, 'Aves', 'jhgfds', 'hgfdxsza', '', '', '', '', 0),
(5, 'Peces', 'Pentabiocare', 'Bio', 'ASD', '120 g', 'Medicamento', 'ASDFGH', 120),
(7, 'Mamiferos', 'NBGVFCDXSZA', 'BVCFXDSZA', '', '', '', '', 0),
(8, 'Reptiles', 'kjhmnbgfvdsx', 'm njhbgfdcxsz', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuario` int(11) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `apaterno` varchar(15) NOT NULL,
  `amaterno` varchar(15) NOT NULL,
  `sexo` varchar(9) NOT NULL,
  `edad` varchar(3) NOT NULL,
  `fechanac` date NOT NULL,
  `telefono` varchar(12) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `contrasenia` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuario`, `nombre`, `apaterno`, `amaterno`, `sexo`, `edad`, `fechanac`, `telefono`, `correo`, `contrasenia`) VALUES
(1, 'Johan', 'Guzman', 'Perez', 'Masculino', '19', '2001-05-26', '55 7502 1505', 'johanguzmpe@gmail.com', 'hola123'),
(2, 'Johana Lizbeth', 'Vizcarra', 'Vazquez', 'Femenino', '19', '2001-04-27', '779 110 4678', 'johanavizcarra12@gmail.com', 'adios123'),
(8, 'Joshua', 'Torres', 'Servin', 'Masculino', '23', '1997-02-27', '55 8745 5874', 'joshua12@gmail.com', 'asd789'),
(9, 'Christian', 'kmjnhgb', 'jnhbgfv', 'Masculino', '21', '1000-01-10', '55 7485 6985', 'johanavizcarra12@gmail.com', 'asd'),
(10, 'Christian', 'kmjnhgb', 'jnhbgfv', 'Masculino', '21', '1000-01-10', '55 7485 6985', 'johanavizcarra12@gmail.com', 'asd');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
