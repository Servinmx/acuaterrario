
// Validar datos del Formulario de Registro
$(document).ready(function() {
    $('#enviar').attr('disabled',true);
    //Validar Telefono
    $('#telefono').keyup(function() {
        var tel1 = $('#telefono').val();
        var tel2 = $('#conftelefono').val();
        if (tel1 == tel2) {
            $('#validarTelefono').text("Los Telefonos Coinciden!").css("color", "green");
            $('#enviar').attr('disabled',false);
        } else {
            $('#validarTelefono').text("Los Telefonos NO Coinciden").css("color", "red");
            $('#enviar').attr('disabled',true);
        }
        if(tel1 == ""){
            $('#validarTelefono').text("No se puede dejar en blanco").css("color", "red");
            $('#enviar').attr('disabled',true);
        }
    });
    $('#conftelefono').keyup(function() {
        var tel1 = $('#telefono').val();
        var tel2 = $('#conftelefono').val();
        if (tel1 == tel2) {
            $('#validarTelefono').text("Los Telefonos Coinciden!").css("color", "green");
            $('#enviar').attr('disabled',false);
        } else {
            $('#validarTelefono').text("Los Telefonos NO Coinciden").css("color", "red");
            $('#enviar').attr('disabled',true);
        }
        if(tel2 == ""){
            $('#validarTelefono').text("No se puede dejar en blanco").css("color", "red");
            $('#enviar').attr('disabled',true);
        }
    });
    //Validar Correo
    $('#correo').keyup(function() {
        var correo1 = $('#correo').val();
        var correo2 = $('#confcorreo').val();
        if (correo1 == correo2) {
            $('#validarCorreo').text("Los Correos Coinciden!").css("color", "green");
            $('#enviar').attr('disabled',false);
        } else {
            $('#validarCorreo').text("Los Correos NO Coinciden").css("color", "red");
            $('#enviar').attr('disabled',true);
        }
        if(correo1 == ""){
            $('#validarCorreo').text("No se puede dejar en blanco").css("color", "red");
            $('#enviar').attr('disabled',true);
        }
    });
    $('#confcorreo').keyup(function() {
        var correo1 = $('#correo').val();
        var correo2 = $('#confcorreo').val();
        if (correo1 == correo2) {
            $('#validarCorreo').text("Los Correos Coinciden!").css("color", "green");
            $('#enviar').attr('disabled',false);
        } else {
            $('#validarCorreo').text("Los Correos NO Coinciden").css("color", "red");
            $('#enviar').attr('disabled',true);
        }
        if(correo2 == ""){
            $('#validarCorreo').text("No se puede dejar en blanco").css("color", "red");
            $('#enviar').attr('disabled',true);
        }
    });
    //Validar Contraseña
    $('#contrasenia').keyup(function() {
        var pass1 = $('#contrasenia').val();
        var pass2 = $('#confcontrasenia').val();
        if (pass1 == pass2) {
            $('#validarContrasenia').text("Las Contraseñas Coinciden!").css("color", "green");
            $('#enviar').attr('disabled',false);
        } else {
            $('#validarContrasenia').text("Las Contraseñas NO Coinciden").css("color", "red");
            $('#enviar').attr('disabled',true);
        }
        if(pass1 == ""){
            $('#validarContrasenia').text("No se puede dejar en blanco").css("color", "red");
            $('#enviar').attr('disabled',true);
        }
    });
    $('#confcontrasenia').keyup(function() {
        var pass1 = $('#contrasenia').val();
        var pass2 = $('#confcontrasenia').val();
        if (pass1 == pass2) {
            $('#validarContrasenia').text("Las Contraseñas Coinciden!").css("color", "green");
            $('#enviar').attr('disabled',false);
        } else {
            $('#validarContrasenia').text("Las Contraseñas NO Coinciden").css("color", "red");
            $('#enviar').attr('disabled',true);
        }
        if(pass2 == ""){
            $('#validarContrasenia').text("No se puede dejar en blanco").css("color", "red");
            $('#enviar').attr('disabled',true);
        }
    });
});