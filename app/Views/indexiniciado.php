<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!--CSS con Bootstrap-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Inicio</title>
    <link rel="stylesheet" href="public/css/base.css">
    <link rel="stylesheet" href="public/css/index.css">

</head>
<body background="https://images.homify.com/c_fill,f_auto,q_0,w_740/v1526483607/p/photo/image/2561426/3.jpg">
<!-- NAVBAR -->
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light" style="background:lightgrey" )
    ">
    <a href="Index.php?controller=usuario&action=inicio"><img src="public/images/logo_gato.png"
                                                              class="img-fluid imagennavbar" alt="Logo de AcuaTerrario" style="margin-right: 50px"></a>

    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="Index.php?controller=usuario&action=inicio">Home<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Index.php?controller=productos&action=productos&id=1">Productos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Index.php?controller=usuario&action=registrar">Agregar Usuario</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Index.php?controller=usuario&action=eliminar">Eliminar Usuario</a>
            </li>


            <li class="nav-item" style="margin-right: 180px">
                <a href="Index.php?controller=usuario&action=inicio" class="nav-link">Cerrar Sesion<i class="fas fa-user"></i></a>
            </li>

        </ul>
        <nav class="navbar navbar-expand-sm ">
            <form class="form-inline" action="/action_page.php">
                <input class="form-control mr-sm-2" type="text" placeholder="Search">
                <button class="btn btn-success" type="submit">Search</button>
            </form>
        </nav>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    </nav>

    <div id="demo" class="carousel slide" data-ride="carousel">

        <!-- Indicators -->
        <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
        </ul>


        <div class="bd-example">
            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="https://cdn.shopify.com/s/files/1/0102/3742/files/Artboard_1_b41fd367-183b-4449-ae0e-3f0f3f05d462.jpg?v=1606326267"
                             class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block">

                        </div>
                    </div>

                </div>
                <div class="carousel-item">
                    <img src="https://cdn.shopify.com/s/files/1/0102/3742/files/Promos_de_Octubre_Mediano.jpg?v=1603813803"
                         class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">

                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    <br>


    <div class="row">
        <div class="col-md-4">
            <div class="thumbnail">
                <a href="/w3images/lights.jpg" target="_blank">
                    <img src="https://static.miscota.com/media/1/banners/banner_1612532815_ciPOfPBd.jpg "
                         alt="Lights" style="width:100%">
                    <div class="caption">

                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail">
                <a href="/w3images/nature.jpg" target="_blank">
                    <img src="https://static.miscota.com/media/1/banners/banner_1613638436_cindPFGF.jpg"
                         alt="Nature" style="width:100%">
                    <div class="caption">

                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail">
                <a href="/w3images/fjords.jpg" target="_blank">
                    <img src="https://static.miscota.com/media/1/banners/banner_1612877186_cih1tJzU.jpg"
                         alt="Fjords" style="width:100%">
                    <div class="caption">

                    </div>
                </a>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-4">
            <div class="thumbnail">
                <a href="/w3images/lights.jpg" target="_blank">
                    <img src="https://static.miscota.com/media/1/banners/banner_1595933366_cio4W7sF.jpg "
                         alt="Lights" style="width:100%">
                    <div class="caption">

                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail">
                <a href="/w3images/nature.jpg" target="_blank">
                    <img src="https://static.miscota.com/media/1/banners/banner_1581342008_ciTeyBne.jpg"
                         alt="Nature" style="width:100%">
                    <div class="caption">

                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="thumbnail">
                <a href="/w3images/fjords.jpg" target="_blank">
                    <img src="https://static.miscota.com/media/1/banners/banner_1613467298_ciP3XJTx.jpg"
                         alt="Fjords" style="width:100%">
                    <div class="caption">

                    </div>
                </a>
            </div>
        </div>
    </div>
    <br>
    <footer class="bg-light text-center text-lg-start ">
        <!-- Grid container -->
        <div class="container p-4 pb-0" >
            <form action="">
                <!--Grid row-->
                <div class="row">
                    <!--Grid column-->
                    <div class="col-auto mb-4 mb-md-0">
                        <p class="pt-2"><strong>Suscríbase a nuestro boletín</strong></p>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-5 col-12 mb-4 mb-md-0">
                        <!-- Email input -->
                        <div class="form-outline mb-4">
                            <input type="email" id="form5Example2" class="form-control"/>
                            <label class="form-label" for="form5Example2">Correo electronico</label>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-auto mb-4 mb-md-0">
                        <!-- Submit button -->
                        <button type="submit" class="btn btn-primary mb-4">
                            Suscribir
                        </button>
                    </div>
                    <!--Grid column-->
                </div>
                <!--Grid row-->
            </form>
        </div>
        <!-- Grid container -->

        <!-- Copyright -->
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
            © 2020 Copyright:
            <a class="text-dark" href="https://mdbootstrap.com/">Equipo Dinamita</a>
        </div>
        <!-- Copyright -->
    </footer>
</div>



</body>
<!--JS con Bootstrap-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script></body>
</html>