<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--CSS con Bootstrap-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <title>Registro</title>
    <link rel="stylesheet" href="public/css/base.css">

    <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="public/js/validarForm1.js"></script>
</head>
<body background="https://static.vecteezy.com/system/resources/previews/001/942/553/non_2x/vivid-color-flow-with-rectangle-frame-background-poster-vector.jpg" >
<div class="container col-md-8">
    <div  id="principal " >
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <h1 style="margin-top: 5px"><center>Registro</center></h1>
            </div>
            <div class="col-md-4"></div>
        </div>
        <br>
        <form action="Index.php?controller=usuario&action=register" method="POST">
            <div class="row">
                <div class="col-md-4">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required maxlength="25">
                </div>
                <div class="col-md-4">
                    <label for="apaterno">Apellido Paterno</label>
                    <input type="text" class="form-control" id="apaterno" name="apaterno" placeholder="Apellido Paterno" required maxlength="15">
                </div>
                <div class="col-md-4">
                    <label for="amaterno">Apellido Materno</label>
                    <input type="text" class="form-control" id="amaterno" name="amaterno" placeholder="Apellido Materno" required maxlength="15">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label for="sexo">Sexo</label>
                    <select class="form-control" id="sexo" name="sexo" required>
                        <option value="Femenino">Femenino</option>
                        <option value="Masculino">Masculino</option>
                        <option value="Otro">Otro</option>
                        <option value="Ninguno">Prefiero no decirlo</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="edad">Edad</label>
                    <input type="text" class="form-control" id="edad" name="edad" placeholder="Edad" required maxlength="3">
                    <div id="ayudaedad" class="form-text">Ingrese solo numeros</div>
                </div>
                <div class="col-md-4">
                    <label for="fechanac">Fecha de nacimiento</label>
                    <input type="date" class="form-control" id="fechanac" name="fechanac" required>
                </div>
            </div>
            <div class="row ayudamargen">
                <div class="col-md-6">
                    <label for="telefono">Telefono</label>
                    <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Telefono (55 1111 1111)" required maxlength="12">
                    <div id="ayudatelefono" class="form-text">Ingrese solo numeros con espacio</div>
                </div>
                <div class="col-md-6">
                    <label for="conftelefono">Confirmar Telefono</label>
                    <input type="text" class="form-control" id="conftelefono" name="conftelefono" placeholder="Confirme su telefono (55 1111 1111)" required maxlength="12">
                    <div id="ayudaconftelefono" class="form-text">Ingrese solo numeros con espacio</div>
                    <!--Validacion de Telefono-->
                    <div id="validarTelefono"></div>
                </div>
            </div>
            <div class="row ayudamargen">
                <div class="col-md-6">
                    <label for="correo">Correo</label>
                    <input type="email" class="form-control" id="correo" name="correo" placeholder="alguien@example.com" required maxlength="50">
                </div>
                <div class="col-md-6">
                    <label for="confcorreo">Confirmar Correo</label>
                    <input type="email" class="form-control" id="confcorreo" name="confcorreo" placeholder="alguien@example.com" required maxlength="50">
                    <!--Validacion de Correo-->
                    <div id="validarCorreo"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label for="contrasenia">Contraseña</label>
                    <input type="password" class="form-control" id="contrasenia" name="contrasenia" placeholder="Contraseña" required maxlength="20">
                </div>
                <div class="col-md-6">
                    <label for="confcontrasenia">Confirmar Contraseña</label>
                    <input type="password" class="form-control" id="confcontrasenia" name="confcontrasenia" placeholder="Confimar Contraseña" required maxlength="20">
                    <!--Validacion de Contraseña-->
                    <div id="validarContrasenia"></div>
                </div>
            </div>
            <div class="row">
                <button class="btn btn-outline-primary btn-lg btn-block" id="enviar" type="submit">Registrar</button>
            </div>
        </form>
        <br>
        <br>
        <br>
    </div>
</div>
<!--JS con Bootstrap-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
</body>
</html>