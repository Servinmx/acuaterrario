<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Productos</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style type="text/css">
        .forma {
            display: inline-flex;
            margin-bottom: 20px;
            border-radius: 4%;
        }
    </style>
</head>
<body background="https://static.vecteezy.com/system/resources/previews/001/942/553/non_2x/vivid-color-flow-with-rectangle-frame-background-poster-vector.jpg" >
<center><h5 class="display-4" face="arial">Editar</h5></center>
<div class="container">
    <form action="Index.php?controller=Productos&action=editar" enctype="multipart/form-data" method="POST" id="registro">
        <input id="id" name="id" type="hidden" value="<?php echo $Producto->id ?>">
        <div class="form-group">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="tipo">Tipo</label>
                    <select id="tipo" name="tipo" class="form-control">
                        <option value="Selecciona">Selecciona</option>
                        <option value="Peces">Peces</option><?php if($Producto -> tipo == "Peces") echo " selected"?>>Peces</option>
                        <option value="Perros">Perros</option><?php if($Producto -> tipo == "Perros") echo " selected"?>>Perros</option>
                        <option value="Gatos">Gatos</option><?php if($Producto -> tipo == "Gatos") echo " selected"?>>Gatos</option>
                        <option value="Reptiles">Reptiles</option><?php if($Producto -> tipo == "Reptiles") echo " selected"?>>Reptiles</option>
                        <option value="Aves">Aves</option><?php if($Producto -> tipo == "Aves") echo " selected"?>>Aves</option>
                        <option value="Mamiferos">Mamiferos</option><?php if($Producto -> tipo == "Mamiferos") echo " selected"?>>Mamiferos</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" value='<?php echo $Producto->nombre ?>'>
                </div>
                <div class="form-group col-md-4">
                    <label for="marca">Marca</label>
                    <input type="text" class="form-control" id="marca" name="marca" value='<?php echo $Producto->marca?>'>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="clave">Clave</label>
                    <input type="text" class="form-control" id="clave" name="clave" value='<?php echo $Producto->clave ?>'">
                </div>
                <div class="form-group col-md-4">
                    <label for="contenido">Contenido</label>
                    <input type="text" class="form-control" id="contenido" name="contenido" value='<?php echo $Producto->contenido?>'>
                </div>
                <div class="form-group col-md-4">
                    <label for="categoria">Categoria</label>
                    <input type="text" class="form-control" id="categoria" name="categoria" value='<?php echo $Producto->categoria?>'>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="descripcion">Descripcion</label>
                    <input type="text" class="form-control" id="descripcion" name="descripcion" value='<?php echo $Producto->descripcion?>'>
                </div>
                <div class="form-group col-md-6">
                    <label for="precio">Precio</label>
                    <input type="number" class="form-control" id="precio" name="precio" value='<?php echo $Producto->precio?>'>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4 offset-md-5">
                    <a href="?controller=Productos&action=productos&id=1" class="btn btn-outline-success">Regresar</a>
                    <button class="btn btn-outline-primary" type="submit">Guardar</button>
                </div>
            </div>
        </div>
    </form>
</div>

</body>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</html>