<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Productos</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style type="text/css">
        .forma {
            display: inline-flex;
            margin-bottom: 20px;
            border-radius: 4%;
        }
        #botoncancelar a{
            color: white;
            padding: 0;
        }
    </style>
</head>
<body background="https://static.vecteezy.com/system/resources/previews/001/942/553/non_2x/vivid-color-flow-with-rectangle-frame-background-poster-vector.jpg" >
<center><h5 class="display-4" face="arial">Registrar</h5></center>
<div class="container">
    <form action="/server/AcuaTerrario/?controller=productos&action=agregar" enctype="multipart/form-data" method="POST" id="registro">
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="tipo">Tipo</label>
                <select id="tipo" name="tipo" class="form-control">
                    <option value="Selecciona">Selecciona</option>
                    <option value="Peces">Peces</option>
                    <option value="Perros">Perros</option>
                    <option value="Gatos">Gatos</option>
                    <option value="Reptiles">Reptiles</option>
                    <option value="Aves">Aves</option>
                    <option value="Mamiferos">Mamiferos</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre del producto">
            </div>
            <div class="form-group col-md-4">
                <label for="marca">Marca</label>
                <input type="text" class="form-control" id="marca" name="marca" placeholder="Marca">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="clave">Clave</label>
                <input type="text" class="form-control" id="clave" name="clave" placeholder="Clave">
            </div>
            <div class="form-group col-md-4">
                <label for="contenido">Contenido</label>
                <input type="text" class="form-control" id="contenido" name="contenido" placeholder="Contenido de producto (Gramos/kilos)">
            </div>
            <div class="form-group col-md-4">
                <label for="categoria">Categoria</label>
                <input type="text" class="form-control" id="categoria" name="categoria" placeholder="categoria">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="descripcion">Descripcion</label>
                <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripcion breve del producto">
            </div>
            <div class="form-group col-md-6">
                <label for="precio">Precio</label>
                <input type="number" class="form-control" id="precio" name="precio" placeholder="Precio">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4 offset-md-5">
                <button class="btn btn-outline-success" type="submit">Agregar</button>
                <button type="button" class="btn btn-danger" id="botoncancelar"><a href="Index.php?controller=usuario&action=iniciado" class="nav-link">Inicio</a></button>
            </div>
        </div>
    </form>
    <?php
    foreach($Productos as $pro){
        echo "<div class='card forma' style='margin: 10px'>".
            "<div class='card-body'>".
            "<h6 class='card-title'><a href='#'>Id: </a>".$pro->id.
            "<h6 class='card-title'><a href='#'>Nombre: </a> ".$pro->nombre."</h6>".
            "<h6 class='card-title'><a href='#'>Marca: </a> ".$pro->marca." </h6>".
            "<h6 class='card-title'><a href='#'>Clave: </a>".$pro->clave."</h6>".
            "<h6 class='card-title'><a href='#'>Contenido: </a>".$pro->contenido."</h6>".
            "<h6 class='card-title'><a href=''>Categoria: </a>".$pro->categoria."</h6>".
            "<h6 class='card-title'><a href=''>Descripcion: </a>".$pro->descripcion."</h6>".
            "<h6 class='card-title'><a href=''>Precio: </a>".$pro->precio."</h6>".
            "<a href='/server/AcuaTerrario/?controller=productos&action=all&id=". $pro->id."' class='btn btn-outline-success'>Editar</a>".
            "<form action='/server/AcuaTerrario/?controller=productos&action=eliminar' class='card-text' enctype='multipart/form-data' method='post' id='formEliminar'>".
            "<input id='id' name='id' type='hidden' value='{$pro-> id}'><br>".
            "<button class='btn btn-outline-danger btnEliminar' type='submit'>Eliminar</button>".
            "</form>".
            "</div></div>";
    }
    ?>
</div>

</body>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</html>