<?php


namespace Models;


class Productos extends Conexion
{
    public $id;
    public $tipo;
    public $nombre;
    public $marca;
    public $clave;
    public $contenido;
    public $categoria;
    public $descripcion;
    public $precio;

    static function all(){
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "SELECT *FROM productos");
        $pre -> execute();
        $res = $pre -> get_result();

        $productos = [];
        while ($producto = $res ->fetch_object(Productos::class)){
            //Devuelve los elementos
            array_push($productos,$producto);
        }
        return $productos;
    }

    static function find($id){
        $me = new Conexion();
        $pre = mysqli_prepare($me -> con, "SELECT *FROM productos WHERE id =?");
        $pre -> bind_param("i", $id);
        $pre -> execute();
        $res = $pre -> get_result();

        return $res -> fetch_object(Productos::class);
    }

    function insertar(){
        $pre = mysqli_prepare($this->con, "INSERT INTO productos (tipo, nombre, marca, clave, contenido, categoria, descripcion, precio) VALUES (?,?,?,?,?,?,?,?)");
        $pre -> bind_param("sssssssi", $this -> tipo, $this -> nombre, $this -> marca, $this -> clave, $this -> contenido, $this ->categoria, $this ->descripcion, $this ->precio);
        $pre -> execute();
        $pre= mysqli_prepare($this->con, "SELECT LAST id");
    }

    function update(){
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "UPDATE productos SET tipo=?, nombre=?, marca=?, clave=?, contenido=?, categoria=?, descripcion=?, precio=? WHERE id=?");
        $pre -> bind_param("sssssssii", $this->tipo, $this->nombre, $this->marca, $this->clave, $this->contenido, $this->categoria, $this->descripcion, $this->precio, $this->id);
        $pre ->execute();
        return true;
    }
    /*
    function update(){
        $me = new Conexion();
        $pre = mysqli_prepare($me->conexion, "UPDATE personas SET nombre=?, apeidoP=?, apeidoM=?, sexo=?, cuatrimestre=? WHERE  id=?");
        $pre -> bind_param("sssssi", $this->nombre, $this->apeidoP, $this->apeidoM, $this->sexo, $this->cuatrimestre, $this->id);
        $pre ->execute();
        return true;
    }
    */
    static function remove ($id){
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "DELETE FROM productos WHERE id=?");
        $pre-> bind_param("i",$id);
        $pre -> execute();
        return true;
    }

}