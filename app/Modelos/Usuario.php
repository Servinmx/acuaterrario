<?php

namespace Models;
class Usuario extends Conexion
{
    public $usuario;
    public $nombre;
    public $apaterno;
    public $amaterno;
    public $sexo;
    public $edad;
    public $fechanac;
    public $telefono;
    public $correo;
    public $contrasenia;

    static function all(){
        $con = new Conexion();
        $pre = mysqli_prepare($con->con, "SELECT * FROM usuarios");
        $pre->execute();
        $res = $pre->get_result();
        while($y=mysqli_fetch_assoc($res)){
            $t[]=$y;
        }
    }

    static function verificarusuario($correo, $contrasenia){
        $con = new Conexion();
        $pre = mysqli_prepare($con->con, "SELECT * FROM usuarios WHERE correo = ? AND contrasenia = ?");
        $pre->bind_param("ss", $correo, $contrasenia);
        $pre->execute();
        $res = $pre->get_result();
        return $res->fetch_object();
    }

    //Funcion para INSERTAR registros a la DB
    function insertar()
    {
        //Pre es para hacer la consulta en base de datos
        $pre = mysqli_prepare($this->con, "INSERT INTO usuarios(usuario, nombre, apaterno, amaterno, sexo, edad, fechanac, telefono, correo, contrasenia) VALUES (?,?,?,?,?,?,?,?,?,?)");
        //bind param le indica que tipos de datos va a insertar
        $pre->bind_param("ssssssssss",$this->usuario, $this->nombre, $this->apaterno, $this->amaterno, $this->sexo, $this->edad, $this->fechanac, $this->telefono, $this->correo, $this->contrasenia);
        //execute ejecutara la consulta
        $pre->execute();
        $this->usuario=$pre;
        require 'app/Views/datos.php';
    }
    //Funcion para ELIMINAR registros en la DB
    function eliminar()
    {
        $conexion = new Conexion();
        $pre = mysqli_prepare($conexion->con, "DELETE FROM `usuarios` WHERE correo = ? AND contrasenia = ?");
        $pre->bind_param("ss", $this->correo, $this->contrasenia);
        $pre->execute();
        $pos = mysqli_prepare($conexion->con, "SELECT * FROM `usuarios` WHERE correo = ? AND contrasenia = ?");
        $pos->bind_param("ss", $this->correo, $this->contrasenia);
        $pos->execute();

        require 'app/Views/datoseliminados.php';
    }
}

?>