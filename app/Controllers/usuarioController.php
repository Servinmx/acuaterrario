<?php
include "app/Modelos/Conexion.php";
include "app/Modelos/Usuario.php";
use Models\Conexion;
use Models\Usuario;

class usuarioController
{
    public function __construct(){

    }

    //Funcion para la VISTA de la pagina principal
    public function inicio(){
        require 'app/Views/index.php';
    }
    //Funcion para la VISTA del Login
    public function login(){
        require 'app/Views/login.php';
    }
    //Funcion para agregar datos
    public function register(){
        if(isset($_POST["nombre"])&& isset($_POST["apaterno"])&& isset($_POST["amaterno"])&& isset($_POST["sexo"])
        && isset($_POST["edad"]) && isset($_POST["fechanac"]) && isset($_POST["telefono"]) && isset($_POST["correo"])
        && isset($_POST["contrasenia"])){
            $usuario = new Usuario();
            $usuario->nombre = $_POST["nombre"];
            $usuario->apaterno = $_POST["apaterno"];
            $usuario->amaterno = $_POST["amaterno"];
            $usuario->sexo = $_POST["sexo"];
            $usuario->edad = $_POST["edad"];
            $usuario->fechanac = $_POST["fechanac"];
            $usuario->telefono = $_POST["telefono"];
            $usuario->correo = $_POST["correo"];
            $usuario->contrasenia = $_POST["contrasenia"];
            $usuario->insertar();
            //header('Location: /AcuaTerrario/Index.php?controller=usuario&action=insertar');
        }else{
            echo "Error, no se pudo insertar el registro";
        }
    }
    public function start(){
        if(isset($_POST["correo"]) && isset($_POST["contrasenia"])) {
            $correo = $_POST["correo"];
            $contrasenia = $_POST["contrasenia"];
            $verificar = Usuario::verificarusuario($correo, $contrasenia);
            if (!$verificar) {
                echo "Error, Usuario no Encontrado";
            } else {
                $_SESSION["usuario"] = "Hola!";
                $_SESSION["correo"] = $_POST["correo"];
                $_SESSION["contrasenia"] = $_POST["contrasenia"];
                require 'app/Views/indexiniciado.php';
            }
        }
    }
    public function iniciado(){
        require 'app/Views/indexiniciado.php';
    }
    public function startoff(){
        if(isset($_SESSION["usuario"])){
            unset($_SESSION["usuario"]);
            unset($_SESSION["correo"]);
            unset($_SESSION["contrasenia"]);
        }
        require 'app/Views/index.php';
    }
    //Funcion para la VISTA mandar a pagina con Formulario
    public function registrar(){
        require 'app/Views/registrar.php';
    }
    public function eliminar(){
        require 'app/Views/eliminar.php';
    }
    //Funcion para eliminar datos
    public function delete(){
        if(isset($_POST["correo"]) && isset($_POST["contrasenia"])){
            $usuario = new Usuario();
            $usuario->correo = $_POST["correo"];
            $usuario->contrasenia = $_POST["contrasenia"];
            $usuario->eliminar();
            //header('Location: /AcuaTerrario/Index.php?controller=usuario&action=insertar');
        }else{
            echo "Error, no se pudo insertar el registro";
        }
    }
}