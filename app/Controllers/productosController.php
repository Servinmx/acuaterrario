<?php

include "app/Modelos/Conexion.php";
include "app/Modelos/Productos.php";
use Models\Conexion;
use Models\Productos;


class productosController
{

    public function __construct()
    {

    }

    public function productos()
    {
        $id = $_GET["id"];

        $Productos = \Models\Productos::all();

        $Producto = \Models\Productos::find($id);
        require_once "app/Views/productos.php";
    }

    public function agregar()
    {
        if (isset($_POST)) {
            $producto = new Productos();
            $producto->tipo = $_POST["tipo"];
            $producto->nombre = $_POST["nombre"];
            $producto->marca = $_POST["marca"];
            $producto->clave = $_POST ["clave"];
            $producto->contenido = $_POST["contenido"];
            $producto->categoria = $_POST["categoria"];
            $producto->descripcion = $_POST["descripcion"];
            $producto->precio = $_POST["precio"];
            $producto->insertar();
            header('Location: /server/AcuaTerrario/Index.php?controller=Productos&action=productos&id=1');
        }
    }

    public function all()
    {
        $id = $_GET["id"];
        var_dump($id);
        $Producto = Productos::find($id);
        require_once "app/Views/verProducto.php";
    }

    public function eliminar()
    {
        $id = $_POST["id"];
        var_dump($id);
        $Producto = Productos::remove($id);
        header('Location: /server/AcuaTerrario/Index.php?controller=productos&action=productos&id=1');
    }

    public function editar()
    {
        if (isset ($_POST)) {
            $id = $_POST["id"];
            $producto = new Productos();
            $producto->tipo = $_POST["tipo"];
            $producto->nombre = $_POST["nombre"];
            $producto->marca = $_POST["marca"];
            $producto->clave = $_POST ["clave"];
            $producto->contenido = $_POST["contenido"];
            $producto->categoria = $_POST["categoria"];
            $producto->descripcion = $_POST["descripcion"];
            $producto->precio = $_POST["precio"];
            $producto -> id = $id;
            /*
            echo $_POST["id"];
            echo $_POST["tipo"];
            echo $_POST["nombre"];
            echo $_POST["marca"];
            echo $_POST["clave"];
            echo $_POST["contenido"];
            echo $_POST["categoria"];
            echo $_POST["descripcion"];
            echo $_POST["precio"];
            */
            $producto->update();
            header('Location: /server/AcuaTerrario/Index.php?controller=Productos&action=all&id='.$id);

        }
    }
    /*
    public function editar (){
        if (isset ($_POST)){
            $id = $_POST["id"];
            $usuario = new Usuario();
            $usuario -> nombre = $_POST["nombre"];
            $usuario -> apeidoP = $_POST["apeidoP"];
            $usuario -> apeidoM = $_POST["apeidoM"];
            $usuario -> sexo = $_POST["sexo"];
            $usuario -> cuatrimestre = $_POST["cuatrimestre"];
            $usuario -> id = $_POST["id"];
            $usuario -> update();
            header('Location: /Pruebas/?controller=Usuario&action=all&id='.$id);
        }
    }
    */
}